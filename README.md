## Impact Factoring Accounts Laravel User Provider

### Requirements:
- Laravel 11
- php 8.2+


### Installation:
Install the package using Composer:
```bash
composer require impact-factoring/accounts-user-provider
```

After package installation, run the following commands:
```bash
php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider"
php artisan migrate
```

### Configuration
Once migration is done, update the following files:


#### In `auth.php`:
Change the user provider driver from 'eloquent' to 'impact-factoring-accounts-user-provider':
```php
'providers' => [
    'users' => [
        'driver' => 'impact-factoring-accounts-user-provider',
        'model' => App\Models\User::class,
],
```
#### In `User.php`:
Extend your local User model with the User model from the package:

```php
use ImpactFactoring\Accounts\Models\User as Authenticatable;
```

Remove the `$hidden` property.

#### In `.env`:
Add the following environment variable:
```bash
IMPACT_FACTORING_ACCOUNTS_BASE_URL=<your_base_url_here>
```
Set this variable to where your local Accounts server is hosted. For example: `http://localhost:8081`.


### Authentication routes
This package contains two authentication routes:

- POST '/login'  -> Process login request
- POST '/logout' -> Process logout request (route name: 'logout')

You are free to use whatever frontend you desire, but processing needs to be done by these two routes.

#### So, make sure you don't have any routes that may be in conflict with these two.







