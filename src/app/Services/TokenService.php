<?php

namespace ImpactFactoring\Accounts\Services;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Cookie;

class TokenService
{
    const TOKEN_NAME = 'impact_factoring_auth_token';

    public function generateCookie(string $token): \Symfony\Component\HttpFoundation\Cookie
    {
        return Cookie::make(
            name: self::TOKEN_NAME,
            value: $token,
            minutes: 60 * 24,
            domain: self::getDomain(),
            secure: app()->isProduction()
        );
    }

    public function getLatestToken(?Authenticatable $user = null): string|false
    {
        return $this->getTokenFromCookie() ?? false;
    }

    private function getTokenFromCookie(): ?string
    {
        return $_COOKIE[self::TOKEN_NAME] ?? null;
    }

    private function getNewestToken(string $storedToken, string $tokenInCookie): string
    {
        $storedTokenId = explode('|', $storedToken)[0];
        $tokenInCookieId = explode('|', $tokenInCookie)[0];

        return $storedTokenId > $tokenInCookieId ? $storedToken : $tokenInCookie;
    }

    private static function getDomain(): string
    {
        return app()->isProduction() ? '.impactfactoring.nl' : 'localhost';
    }
}
