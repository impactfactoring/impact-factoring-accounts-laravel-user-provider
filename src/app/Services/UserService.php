<?php

namespace ImpactFactoring\Accounts\Services;

use Illuminate\Contracts\Auth\Authenticatable;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\Employee;

class UserService
{
    public function buildModel(array $userData, string $model, $token = null): Authenticatable
    {
        $user = $model::updateOrCreate(
            ['email' => $userData['email']],
            [
                'first_name'  => $userData['first_name'],
                'last_name'   => $userData['last_name'],
                'accounts_id' => $userData['id'],
                'auth_token'  => $userData['auth_token'],
            ]
        );

        $this->reAssignPermissions($user, $userData['permissions']);
        $this->reAssignRoles($user, $userData['role']);

        return $user;
    }

    private function reAssignPermissions(Authenticatable $user, array $permissions): void
    {
        if (!method_exists($user, 'syncPermissions') || !method_exists($user, 'givePermissionTo')) {
            return;
        }

        $user->syncPermissions();

        foreach ($permissions as $permission) {
            $permission = Permission::firstOrCreate(['name' => $permission]);
            $user->givePermissionTo($permission);
        }
    }

    private function reAssignRoles(Authenticatable $user, array $roles): void
    {
        if (!method_exists($user, 'syncRoles') || !method_exists($user, 'assignRole')) {
            return;
        }

        $user->syncRoles();

        foreach ($roles as $role) {
            $role = Role::firstOrCreate(['name' => $role]);
            $user->assignRole(['name' => $role]);
        }
    }
}
