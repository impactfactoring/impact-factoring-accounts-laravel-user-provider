<?php

namespace ImpactFactoring\Accounts\Providers;

use Illuminate\Cookie\Middleware\EncryptCookies;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use ImpactFactoring\Accounts\Client\ImpactFactoringAccountsClient;
use ImpactFactoring\Accounts\Services\TokenService;
use ImpactFactoring\Accounts\Services\UserService;

class AccountsServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/impact-factoring-accounts.php', 'impact-factoring-accounts'
        );
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/permission.php', 'impact-factoring-accounts'
        );

        $this->app->afterResolving(EncryptCookies::class, function ($middleware) {
            $middleware->disableFor(TokenService::TOKEN_NAME);
        });

        Auth::provider('impact-factoring-accounts-user-provider', function ($app, array $config) {
            return new AccountsUserProvider($config);
        });

        $token = (new TokenService())->getLatestToken();

        if ($token) {
            $user = (new ImpactFactoringAccountsClient(Config::get('impact-factoring-accounts.base_url')))->getUserByToken(token: $token);

            if ($user) {
                Auth::login((new UserService())->buildModel(
                    $user,
                    Config::get('auth.providers.users.model'),
                    $token
                ));
            }
        }

        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
    }
}
