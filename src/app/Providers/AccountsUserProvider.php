<?php

namespace ImpactFactoring\Accounts\Providers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Config;
use ImpactFactoring\Accounts\Client\ImpactFactoringAccountsClient;
use ImpactFactoring\Accounts\Services\TokenService;
use ImpactFactoring\Accounts\Services\UserService;

class AccountsUserProvider implements UserProvider
{
    protected string $model;
    protected ImpactFactoringAccountsClient $client;
    protected TokenService $tokenService;
    protected UserService $userService;
    protected ?array $user = null;

    public function __construct(array $config)
    {
        $this->model = $config['model'];
        $this->client = new ImpactFactoringAccountsClient(Config::get('impact-factoring-accounts.base_url'));
        $this->tokenService = new TokenService();
        $this->userService = new UserService();
    }

    public function retrieveById($identifier): ?Authenticatable
    {
        $user = $this->user[$identifier] = $this->user[$identifier] ?? $this->model::findOrFail($identifier);
        $token = $this->tokenService->getLatestToken($user);
        $userData = $this->client->getUserByToken($user->accounts_id, $token);

        return $userData ? $this->userService->buildModel($userData, $this->model) : null;
    }

    public function retrieveByToken($identifier, $token)
    {}

    public function updateRememberToken(Authenticatable $user, $token): Authenticatable
    {
        return $user;
    }

    public function retrieveByCredentials(array $credentials): ?Authenticatable
    {
        if (isset($credentials['email']) && isset($credentials['password']) && $response = $this->client->login($credentials)) {
            return $this->userService->buildModel($response, $this->model);
        }

        return null;
    }

    public function validateCredentials(Authenticatable $user, array $credentials): bool
    {
        if (!$token = $this->tokenService->getLatestToken($user)) {
            return false;
        }

        return $this->client->authenticate($token);
    }

    public function rehashPasswordIfRequired(Authenticatable $user, array $credentials, bool $force = false): void
    {
    }
}
