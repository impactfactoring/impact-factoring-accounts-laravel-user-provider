<?php

namespace ImpactFactoring\Accounts\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use ImpactFactoring\Accounts\Client\ImpactFactoringAccountsClient;
use ImpactFactoring\Accounts\Services\TokenService;

class AuthController extends Controller
{
    public function logout(): void
    {
        $user = Auth::user();

        //        Cache::forget($user->accounts_id);

        if ((new ImpactFactoringAccountsClient(Config::get('impact-factoring-accounts.base_url')))->logout((new TokenService())->getLatestToken($user))) {
            $user->auth_token = null;
            $user->save();
            Auth::logout();
        }
    }
}
