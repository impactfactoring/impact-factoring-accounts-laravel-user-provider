<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {

            if (Schema::hasColumn('users', 'password')) {
                $table->dropColumn('password');
            }

            if (Schema::hasColumn('users', 'remember_token')) {
                $table->dropColumn('remember_token');
            }

            if (Schema::hasColumn('users', 'name')) {
                $table->dropColumn('name');
            }

            if (Schema::hasColumn('users', 'email_verified_at')) {
                $table->dropColumn('email_verified_at');
            }

            $table->bigInteger('accounts_id')->unsigned()->index()->after('id');
            $table->string('first_name')->after('accounts_id')->nullable();
            $table->string('last_name')->after('first_name')->nullable();
            $table->string('auth_token')->after('last_name')->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('accounts_id');
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('auth_token');
        });
    }
};
