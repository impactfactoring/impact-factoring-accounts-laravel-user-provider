<?php

use Illuminate\Support\Facades\Route;
use ImpactFactoring\Accounts\Controllers\AuthController;

Route::middleware('auth')->group(function () {
    Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
});
